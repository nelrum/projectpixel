# **VoxelLike** - ***A Rogue Like Experience***

![Voxel Like Logo](img/logo.png "Title Text")

Different challenges awaits you in the... **Infinity Tower**

# Authors

- **Luis Angel Macias de la Cruz** --- Main Developer, Artist, Game Design and Story Script

# Install

- Clone the repository and execute ***VoxelLike Setup.exe***
